from typing import Union, Tuple
from flask import Flask, request, Response
from flask_cors import CORS
import numpy as np
import json
import os
import cv2
from classOperator import Operator

app = Flask(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

port = os.environ["PORT"]

# operator = Operator('path/to/model', 'path/to/weights')  # ok, path to files of the neural network, insert here!
operator = Operator() # debug

# try: # ok
#	model, weights = operator.load_model()
# except ValueError:
#	raise FileNotFoundError(f'{operator.load_model()}')
 

@app.route('/process_image', methods=['POST'])
def main():
        try:
                # global operator, model, weights # ok
                global operator # debug
                image = operator.receive_image()
                # assert isinstance(image, ) # debug
                # prediction = operator.make_prediction(model, image) # ok
                # prediction = operator.process_prediction(prediction) # ok
                # response = {'message': f'estimated price (rubles): {prediction}'} # ok
                response = {'type(img)': f'{type(image)}'} # debug, 'type(image)' or whatever you need.
                response = json.dumps(response) # ok
                return Response(response=response, status=200, mimetype='application/json')
        except Exception:
                return Response(status=400)


@app.route('/', methods=['GET'])
def test():
	response = {'message': 'service is working'}
	response = json.dumps(response)
	return Response(response=response, status=200, mimetype='application/json')


if __name__ == "__main__":
    app.run('0.0.0.0', os.environ.get('PORT'), 5000)
