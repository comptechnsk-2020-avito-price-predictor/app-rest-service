import json
from typing import Union, Tuple
import h5py
from flask import Flask, Response, request
import numpy as np
import cv2


ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])


class Operator:

	#def __init__(self, model_path: str, weights_path: str): # ok
	#	self.model_path = model_path
	#	self.weights_path = weights_path
	def __init__(self): # debug
		pass


	def _allowed_file(filename):
		global ALLOWED_EXTENSIONS
		return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


	def receive_image(self):
		"""Recieves user's image, returns this image.
		"""
		r = request
		image = r.files['file']
##		if image and self._allowed_file(image.filename):
		if image:
			nparr = np.fromstring(image.read(), np.uint8)
			image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
			return image
		else:
			raise Exception


	def load_model(self):
		"""Loads a neural network model from files,
		returns a model and its weights.
		"""
		try:
			with open(self.model_path, 'r') as m, open(self.weights_path, 'r') as w:
				model = json.dumps(m)
				weights = h5py.File(w, 'r')
				return {model, weights}
		except FileNotFoundError:
			return f'Not Found {m.name} or/and {w.name} file(s)'


	def make_prediction(self, model, image):
		"""Makes a prediction using neural network,
		returns the prediction.
		"""
		pred = model.predict(image)
		# type(pred) = ?
		return pred


	def process_prediction(self, pred: Union[int, Tuple[int, int]]):
		"""Makes prediction value '--all-human-readable'.
		"""
		if isinstance(pred, tuple):
			return f'{pred[0]} ... {pred[1]}'
		else:
			return str(pred)
